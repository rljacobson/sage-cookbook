\section{Lists and Tuples}
%Lists in Sage are native Python constructs. 
\subsection{Basic List Manipulation}
%% append, pop, sublists, etc. %%
\noindent
\begin{tabularx}{\textwidth}{X p{5cm} p{3cm}}
\sheader

\srow{
Defining a list.
}{
my\_list = [1, 2, 3, 4, 5]
}{}

\srow{
Length of a list.
}{
len(my\_list)
}{
5
}

\srowmulti{
Accessing list elements.
}{
my\_list[0]
}{
1
}

\srow{}{
my\_list[-2]
}{
5
}

\srow{
Sublist starting at index $m$ and ending before index $n$.
}{
my\_list[1:4]
}{
[2, 3, 4]
}

\srowmulti{
Sublist starting at index $m$ until the end.
}{
my\_list[1:]
}{
[2, 3, 4, 5]
}

\srow{
}{
my\_list[-3:]
}{
[3, 4, 5]
}

\srowmulti{
Sublist from the beginning and ending before index $n$.
}{
my\_list[:3]
}{
[1, 2, 3]
}

\srow{
}{
my\_list[:-1]
}{
[1, 2, 3, 4]
}

\srowmulti{
Assigning value to element at location\ldots
}{
my\_list[3]=2 \newline
show(my\_list)
}{
[1, 2, 3, 2, 5]
}

\srow{
\ldots or multiple values at once.
}{
lst2 = [8, 9, 10, 11] \newline
lst2[1:] = 7, 6, 5 \newline
show(lst2)
}{
[8, 7, 6, 5]
}

\srow{
Appending an element to a list.
}{
my\_list.append(pi) \newline
show(my\_list)
}{
[1, 2, 3, 2, 5, $\pi$]
}

\srow{
Insert a value before index $m$.
}{
my\_list.insert(1, e) \newline
show(my\_list)
}{
[1, $e$, 3, 2, 5, $\pi$]
}

\srowmulti{
Pop an element off of the end of a list. 
}{
show(my\_list.pop()) \newline
show(my\_list)
}{
$\pi$ \newline
[1, $e$, 2, 3, 2, 5]
}

\srow{
Count the occurrences of a value in a list.
}{
my\_list.count(2)
}{
2
}

\srow{
Remove the first occurrence of a value from a list.
}{
my\_list.remove(2) \newline
show(my\_list)
}{
[1, $e$, 3, 2, 5]
}

\srow{
Append the elements of a list onto another list.
}{
my\_list.extend([9, 8, 7]) \newline
show(my\_list)
}{
[1, $e$, 3,2,5,9,8,7]
}

\srow{
Reverse a list in place.
}{
my\_list.reverse() \newline
show(my\_list)
}{
[7,8,9,5,2,3,e,1]
}

\srowmulti{
Sort a list in place.
}{
my\_list.sort() \newline
show(my\_list)
}{
[1,2,e,3,5,7,8,9]
}

\srow{}{
my\_list.sort(reverse=True) \newline
show(my\_list)
}{
[9,8,7,5,3,e,2,1]
}

\srow{
Find the index of the first occurrence of a value. If the value is not found, an
error is raised. }{
my\_list.index(7)
}{
2
}

\end{tabularx}

\vf

\subsection{List Creation and List Comprehensions}

%% range, srange, xrange, xsrange, [..] %%
\noindent
\begin{tabularx}{\textwidth}{X p{5cm} p{3cm}}
\sheader

\srowmulti{
List containing a range of Python numbers.
}{
range(3)
}{
[0, 1, 2]
}

\srowmulti{}{
range(1, 4)
}{
[1, 2, 3]
}

\srow{}{
range(3, 11, 2)
}{
[3, 5, 7, 9]
}

\srowmulti{
List containing a range of Sage numbers.
}{
srange(3)
}{
[0, 1, 2]
}

\srow{}{
srange(.2, pi, .9)
}{
[0.200000000000000, 1.10000000000000, 2.00000000000000, 2.90000000000000]
}

\srowoutless{
Create a list of Python numbers without allocating memory for it.
}{
xrange(10000) \newline
xrange(15000, 17000, 2)
}

\srowoutless{
Create a list of Sage numbers without allocating memory for it.
}{
xsrange(10000) \newline
xsrange(15000, 17000, pi)
}

\srowmulti{
An srange alternative. (Includes endpoint.)
}{
[0, pi/3, .., pi]
}{
[0, 1/3*pi, 2/3*pi, pi]
}

\srow{
}{
[0, pi/5, .., pi] == srange(0, pi, pi/5, include\_endpoint=True)
}{
True
}

\end{tabularx}

\vtspace

Sage/Python has an idiom similar to the \emph{map} function of
other languages called list comprehensions. (It also has \emph{map}.) The
variable name is local to the scope of the square brackets.

\noindent
\begin{tabularx}{\textwidth}{X p{5cm} p{3cm}}
\sheader

\srow{
List from a list comprehension.
}{
squares = [x\caret 2 for x in srange(5)] \newline
show(squares)
}{
[0,1,4,9,16]
}

\srowmulti{
Construct a list using values from two different lists.
}{
[[x, y] for x in [1,2,3] for y in [2, 3]]
}{
[[1, 2], [1, 3], [2, 2], [2, 3], [3, 2], [3, 3]]
}

\srow{
Exclude the diagonal.
}{
[[x, y] for x in [1, 2, 3] for y in [2, 3] if x != y]
}{
[[1, 2], [1, 3], [2, 3], [3, 2]]
}

\srow{
Nested list comprehension to compute the transpose of an array of numbers.
(This is not how you represent a matrix or compute a matrix transpose in Sage.)
}{ 
num\_array = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]] \newline
result = [[row[i] for row in num\_array] for i in range(4)] \newline
show(result)
}{
[[1, 5, 9], [2, 6, 10], [3, 7, 11], [4, 8, 12]]
}
\end{tabularx}

\vtspace

The list is one of two sequence types. The other is the tuple. The element
access and ``sub-tuple'' access, called \emph{slicing}, is the same for tuples
as it is for lists. Tuples, however, are immutable.

\noindent
\begin{tabularx}{\textwidth}{X p{5cm} p{3cm}}
\sheader

\srowoutless{
Defining a tuple.
}{
tup = (1, 2, 3, 4) \newline
tup2 = 5, 6, 7
}

\srowmulti{
Element access and slicing. (Same as with lists.)
}{
tup[3]
}{
4
}

\srow{}{
tup[2:]
}{
(3, 4)
}

\srow{
Singleton tuple.
}{
tup = 3, \newline
show(tup)
}{
(3)
}

\srow{
Tuple unpacking.
}{
x, y, z = (e, pi, 1729) \newline
show(x) \newline
show(y) \newline
show(z)
}{
output
}
\end{tabularx}


\vf